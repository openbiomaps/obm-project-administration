import axios from "axios";
import Taxon from "../../models/Taxon";

const url = "http://milvus.openbiomaps.org/projects/teszt/ajax";
const namespaced = true;
const states = {
  taxons: [],
  translations: {}
};

const getters = {
  allTaxons: state => state.taxons,
  allTranslations: state => state.translations
};

const actions = {
  init({ dispatch }) {
    return Promise.all([
      dispatch("fetchTaxons"),
      dispatch("fetchTranslations")
    ]);
  },
  async fetchTranslations({ commit }) {
    try {
      const response = await axios.get(url, {
        params: {
          taxonomy: "translations"
        }
      });
      commit("setTranslations", response.data);
    } catch (e) {
      if (e.response) {
        console.log(e.request);
      } else if (e.request) {
        console.log(e.request);
      } else {
        console.log(e);
      }
    }
  },
  async fetchTaxons({ commit }) {
    try {
      const response = await axios.get(url, {
        params: {
          taxonomy: "load_children",
          taxonId: 0,
          limit: 20,
          offset: 0
        }
      });
      if (response.data.status != "success") {
        throw new Error(response.data.message);
      }
      commit(
        "setTaxons",
        Object.values(response.data.data).map(t => new Taxon(t))
      );
    } catch (e) {
      if (e.response) {
        console.log(e.request);
      } else if (e.request) {
        console.log(e.request);
      } else {
        console.log(e);
      }
    }
  },
  async updateTaxons({ commit }, value) {
    console.log(value);
    value.forEach(function(tax, idx) {
      tax.ord = idx;
    });
    commit("setTaxons", value);
  }
};

const mutations = {
  setTaxons: (state, taxons) => (state.taxons = taxons),
  setTranslations: (state, translations) => (state.translations = translations)
};

export default {
  namespaced,
  states,
  getters,
  actions,
  mutations
};

import Vue from "vue";
import Vuex from "vuex";
import linnaeus from "./modules/linnaeus";

Vue.use(Vuex);

export default new Vuex.Store({
  modules: {
    linnaeus
  }
});

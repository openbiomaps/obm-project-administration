import axios from "axios";

export default class Word {
  constructor(
    wid = null,
    word = "",
    status = "undefined",
    lang = "",
    taxonDb = 0,
    taxonId = null
  ) {
    this.wid = wid;
    this.word = word;
    this.status = status;
    this.lang = lang;
    this.taxonDb = taxonDb;
    this.taxonId = taxonId;
  }
  async save() {
    try {
      if (!this.lang || !this.word) {
        throw new Error("word and lang can't be empty!");
      }
      const resp = await axios.post("ajax", {
        taxonomy: "save_word",
        word: this
      });
      const result = JSON.parse(resp);
      if (result.status !== "success") {
        throw result.message;
      }
      this.wid = result.data.wid;
      this.taxonId = result.data.taxonId;
      return true;
    } catch (e) {
      console.log(e);
      return false;
    }
  }
  async delete() {
    try {
      const resp = await axios.post("ajax", {
        taxonomy: "delete_word",
        word: this
      });
      const result = JSON.parse(resp);
      if (result.status !== "success") {
        throw result.message;
      }
      return true;
    } catch (e) {
      console.log(e);
      return false;
    }
  }
}

import axios from "axios";
import Word from "./Word";

export default class Taxon {
  constructor(t = { taxonId: null, words: [], parentId: null, ord: 0 }) {
    // taxonId = null, accepted = [], synonym = [], misspelled = [], undef = [], parentId = null) {
    this.taxonId = t.taxonId;
    this.accepted = [];
    this.synonym = [];
    this.misspelled = [];
    this.undefined = [];
    this.parent = null;
    this.children = [];
    this.ord = 0;

    t.words.forEach(w => {
      if (
        ["accepted", "synonym", "misspelled", "undefined"].indexOf(w.status) >=
        0
      ) {
        this[w.status].push(
          new Word(w.wid, w.word, w.status, w.lang, w.taxonDb, w.taxonId)
        );
      }
    });

    this.parentId = t.parentId;
    this.words = t.words.map(w => w.word);
  }

  accepted_name() {
    return this.accepted.length ? this.accepted[0].word : this.words[0];
  }

  async saveParent() {
    try {
      let resp = await axios.post("ajax", {
        taxonomy: "update_parent",
        taxonId: this.taxonId,
        parentId: this.parentId
      });
      resp = JSON.parse(resp);
      if (resp.status !== "success") {
        throw new Error(resp.message);
      }
      this.loadParent();
    } catch (e) {
      console.log(e);
    }
  }

  async loadParent() {
    if (this.parentId) {
      try {
        const resp = await axios.get("ajax", {
          taxonomy: "load",
          ids: [this.parentId]
        });
        this.parent = new Taxon(resp[0]);
      } catch (e) {
        console.log(e);
      }
    }
  }

  async loadChildren() {
    try {
      const resp = await axios.get("ajax", {
        taxonomy: "load_children",
        taxonId: this.taxonId
      });
      if (resp.status !== "success") {
        throw new Error(resp.message);
      }
      this.children = Object.values(resp.data).map(t => new Taxon(t));
    } catch (e) {
      console.log(e);
    }
  }
}

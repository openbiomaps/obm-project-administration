import Vue from "vue";
import VueRouter from "vue-router";
import Home from "../components/Home";
import Linnaeus from "../components/Linnaeus";
import store from "../store";

Vue.use(VueRouter);

const routes = [
  {
    path: "/linnaeus",
    component: Linnaeus,
    beforeEnter: (to, from, next) => {
      store.dispatch("linnaeus/init").then(next);
    }
  },
  {
    path: "/",
    component: Home
  }
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes
});

export default router;

module.exports = {
  publicPath:
    process.env.NODE_ENV === "production"
      ? "/projects/teszt/project-admin/"
      : "/",
  transpileDependencies: ["vuetify"]
};
